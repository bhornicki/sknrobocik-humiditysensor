# zad2 Odczyt sensora poprzez ADC, wysyłanie danych UARTem
Sensor HIH5030

Kod jest wysoce zintegrowany, więc nie wydzieliłem osobnej klasy do obsługi sensora. 
Dodanie kolejnego sensora wymaga dodania kolejnego pomiaru do ADC regular conversion i wielu drobnych zmian.

## Timer
- Zakres wartości 0-255
- Około 120Hz
    - prescaler 2343
    - 72*10^6 / (2343+1) / (255+1) ~= 119.98

## USART
- w sumie tylko wysyłanie
- baud 115200
- 8n1
    - 8 bitów
    - brak parzystości
    - 1 bit stopu
- inaczej mówiąc klasyka niczym solnik

## ADC
- sampling rate 71.5, nie spieszy nam się
- zagar 12MHz
- 71.5/12 ~ 6us
- pomiar grupy:
    - pin sensora wilgotności
    - napięcie referencyjne 1V2
- wykorzystuje DMA

## Zamiana napięcia na wilgotność
Opiszę sytuację w prosty sposób: "Good enough for ~~Australia~~ Poland".

Zamiana wykorzystuje funkcję map ukradzioną z bibliotek Arduino.

Wartości odczytane z wykresu "Figure 3. Typical Output Voltage vs Relative Humidity (At 25 °C and 3.3 Vdc.)" z noty katalogowej sensora HIH5030:

| punkt | napięcie (V) | wilgotność (%RH) |
| --- | --- | --- |
| 0   | 0.5 | 0   |
| 1   | 1.5 | 50  |
| 2   | 2   | 70  |

Mniej więcej w okolicach 40-50RH wykres się przełamuje.
Dlatego zamiana jest podzielona na dwa podzakresy, z podziałem w punktcie 1 odpowiadającym wartości 50%RH.

Pierwszy zakres mapowany jest z wykorzystaniem wartości punktów 0 i 1, z dolną granicą wyniku 0%RH. 
Drugi korzysta z punktów 1 i 2, z górną granicą 100%RH.


Najlepszym wyjściem byłoby przeprowadzenie testów w stałej, docelowej temperaturze środowiska pracy sensora, przy kontrolowanej, zmiennej wartości wilgotności. 
Należałoby zbadać liniowość uzyskanych wyników, ustalić ilość stref mapowania i nowe wartości dla punktów.

Uproszczone testy działania kodu mikrokontrolera można wykonać zastępując wyjście sensora środkową nóżką potencjometru, pozostałe podłączając do 3V3 i GND - układ dzielnika napięcia.

## Opis przebiegu
1. (pętla główna) sprawdzenie czy minęły 2s 
i uruchomienie odczytu ADC
2. Odczyt grupy ADC,
DMA przesyła wyniki do zmiennych globalnych :disappointed:
3. (HAL_ADC_ConvCpltCallback) DMA przekazało dane, 
więc ustawiamy globalną flagę informującą o danych do przetworzenia
4. (pętla główna) sprawdzenie flagi z pkt. 3, 
przetworzenie wartości ADC na napięcia,
obliczenie wilgotności,
włączenie alarmu LED,
wysłanie wiadomości po uarcie
5. (pętla główna) obsługa wartości PWM diody

## Opis schematu nazewnictwa
Projekt korzysta z pochodnej CamelCase:

- NAZWY_STAŁYCH
- gNazwyZmiennychGlobalnych
- nazwyZmiennychLokalnych
- NazwyKlas
- NazwyDeklarowanychStruktur_TypeDef

## TODO:
- [x] sprawdzić czy potrafię w liczenie okresu timera - CKD nie wpływa na częstotliwość!
- [x] sprawdzić jak się ma stary spór uint8_t vs bool - wygląda że zajmują tyle samo pamięci
- [ ] przetestować poprawnie poprawke vref
- [ ] przenieść zarządzanie PWM do nadpisanej HALowskiej funkcji systick, albo do callbacka timera
- [x] wysyłanie danych po UART
    - [x] 0.5Hz, "cykl" raz na 2s
    - [x] nadpisany printf
- [x] wzór
    - [x] nieliniowość odczytu
    - [ ] wpływ temperatury
    - [ ] odczyt temperatury
    - [x] korekta Vref
- [x] alarm przy danej wartości
    - [x] wartość progowa
    - [x] płynne rozjaśnianie i przyciemnianie diody PWMem 
    - [x] zmiana tekstu po uarcie
- [x] porządek w kodzie
    - [x] nazwy zmiennych
    - [x] komentarze
- [x] PCB
    - [x] płytka wyeksportowana do PDF
    - [x] dodatkowe
        - [x] model 3D
        - [x] BOM
        - [x] Może nareszcie jest okazja żeby Altium ogarnąć xD
        - [ ] Poprawić footprint kondensatora żeby DRC nie krzyczał n.t. soldermask pad clearance
        - [ ] Dodać brakujące kondensatory z reference designu