/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <cinttypes>
#include <cstdio>
#include <cmath>
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
struct MapPair_TypeDef
{
	float voltage;
	float rh;
};
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;
DMA_HandleTypeDef hdma_adc1;

TIM_HandleTypeDef htim1;

UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */
uint16_t gAdc[2];	///< space for ADC regular conversion result
bool gAdcMeasCplt;	///< flag set when ADC measurement is completed
float gHumidity;		///< humidity value converted from ADC readout of HIH5030 sensor
uint8_t gPwm;		///< alarm LED 8bit PWM value. 0 is reserved to off state, all other values will pulse
bool gPwmPhaseRising;	///< alarm LED state of pulse. Toggled when gPwm reaches border value - 0 or 256 (so another 0)

constexpr float HUMIDITY_ALARM=60.0;	///< threshold value, when gHumidity crosses it, alarm wil begin

/**
 * set of pairs of values used to map humidity sensor voltage to relative humidity.
 * Mapping is split to two ranges, with POINT1 voltage being the threshold between them.
 */
const struct
{
	const MapPair_TypeDef POINT0 = {0.5, 0};
	const MapPair_TypeDef POINT1 = {1.5, 50};
	const MapPair_TypeDef POINT2 = {2  , 80};
} SENSOR_MAP;

//Constants
constexpr float VREF=1.2;	///< internal voltage reference value
constexpr float VCC=3.3;		///< MCU supply voltage
constexpr uint16_t ADC_VAL_MAX = std::pow(2,12) - 1;	///< maximal value which ADC can read, 12 bits.
constexpr uint32_t DELAY_MEASURE = 2000;	///< time between ADC humidity measures and sending the messages
constexpr uint32_t DELAY_PWM_CHANGE = 2;	///< time between alarm LED PWM next pulse value changes
constexpr uint8_t ADC_INDEX_SENSOR = 0;		///< ADC regular conversion rank of HIH5030 readout
constexpr uint8_t ADC_INDEX_VREF = 1;		///< ADC regular conversion rank of internal voltege reference readout
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_ADC1_Init(void);
static void MX_TIM1_Init(void);
static void MX_USART2_UART_Init(void);
/* USER CODE BEGIN PFP */
const float map(const float x, const float in_min, const float in_max, const float out_min, const float out_max);
const float map(const float x, const MapPair_TypeDef point_lower, const MapPair_TypeDef point_upper);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_ADC1_Init();
  MX_TIM1_Init();
  MX_USART2_UART_Init();
  /* USER CODE BEGIN 2 */
  uint32_t timePrevMeas=0;	//last ADC measurement call time
  uint32_t timePrevPwm=0;	//last alarm PWM value change call time

  HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_1);	//start alarm LED PWM
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
	  /*
	   * First step - order Direct Memory Access module to read ADC regular conversion and save
	   * the results to the gAdc array.
	   * ADC readout and result transfer is made in the background, non-blocking.
	   * Will repeat after DELAY_MEASURE time.
	   */
	  if(HAL_GetTick()-timePrevMeas>DELAY_MEASURE)
	  {
		  timePrevMeas=HAL_GetTick();
		  HAL_ADC_Start_DMA(&hadc1, (uint32_t*) gAdc, 2);
	  }

	  /*
	   * Wait for ADC finished flag, clear it and process ADC data.
	   */
	  if(gAdcMeasCplt)
	  {
		  gAdcMeasCplt = false;
		  /*
		   * Translate ADC bit representation to voltage float.
		   * Account for differences between internal voltage reference true value and read one
		   */
		  float readVref = VCC*gAdc[ADC_INDEX_VREF]/ADC_VAL_MAX;
		  float readSensor = VCC*readVref/VREF*gAdc[ADC_INDEX_SENSOR]/ADC_VAL_MAX;
		  const uint32_t timestamp = HAL_GetTick()/1000;	//prepare time in seconds

		  /*
		   * Map sensor voltage to relative humidity and clamp range to [0-100]
		   * Process is split to two ranges to account for non-linearity of sensor
		   * POINT1 is threshold voltage which decides which range we will use
		   */
		  if(readSensor <= SENSOR_MAP.POINT1.voltage)
		  {
			  gHumidity = map(readSensor, SENSOR_MAP.POINT0, SENSOR_MAP.POINT1);
		  	  if(gHumidity<0)
		  		  gHumidity=0;
		  }
		  else
		  {
			  gHumidity = map(readSensor, SENSOR_MAP.POINT1, SENSOR_MAP.POINT2);
			  if(gHumidity>100)
				  gHumidity=100;
		  }

		  /*
		   * Send message via UART, enable/disable LED alarm
		   */
		  if(gHumidity<HUMIDITY_ALARM)
		  {
			  gPwm=0;	//0 means disabled
			  __HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_1, gPwm);	//turn off LED
			  printf("[%lu] SENSOR: %3.2fV %5.2f%%RH\r\n",timestamp,readSensor,gHumidity);	//8KB flash for float support? :O
		  }
		  else
		  {
			  if(gPwm==0)
				 gPwm=1;	//anything means enabled
			  printf("[%lu]\tPotezna wichura, lamiac duze drzewa, trzcina zaledwie tylko kolysze.\r\n"
					 "\tUwazaj! Uwazaj! Uwazaj! Uwazaj! SENSOR: %3.2fV %5.2f%%RH\r\n",
					 timestamp,readSensor,gHumidity);
		  }
	  }

	  /*
	   * Alarm LED pulse code.
	   * Processed when PWM is active and DELAY_PWM_CHANGE time passes.
	   *
	   * Depending on phase - - luminosity rising or falling, the PWM value is respectively
	   * incremented/decremented.
	   * When new value will be 0, we revert it to previous one to preserve 0 = PWM off assumption,
	   * also the phase is changed to opposing one
	   */
	  if(gPwm)
		  if(HAL_GetTick()-timePrevPwm>DELAY_PWM_CHANGE)
		  {
			  timePrevPwm=HAL_GetTick();
			  if(gPwmPhaseRising)
			  {
				  if(!++gPwm)
				  {
					  --gPwm;
					  gPwmPhaseRising= false;
				  }
			  }
			  else
			  {
				  if(!--gPwm)
					  {
						  ++gPwm;
						  gPwmPhaseRising= true;
					  }
			  }
			   __HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_1, gPwm);	//update PWM output
		  }

  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_ADC;
  PeriphClkInit.AdcClockSelection = RCC_ADCPCLK2_DIV6;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */
  /** Common config
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ScanConvMode = ADC_SCAN_ENABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 2;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Regular Channel
  */
  sConfig.Channel = ADC_CHANNEL_0;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLETIME_71CYCLES_5;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Regular Channel
  */
  sConfig.Channel = ADC_CHANNEL_VREFINT;
  sConfig.Rank = ADC_REGULAR_RANK_2;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief TIM1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM1_Init(void)
{

  /* USER CODE BEGIN TIM1_Init 0 */

  /* USER CODE END TIM1_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};
  TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 2343;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 255;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
  sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
  sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
  sBreakDeadTimeConfig.DeadTime = 0;
  sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
  sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
  sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
  if (HAL_TIMEx_ConfigBreakDeadTime(&htim1, &sBreakDeadTimeConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */

  /* USER CODE END TIM1_Init 2 */
  HAL_TIM_MspPostInit(&htim1);

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Channel1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel1_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin : PC13 */
  GPIO_InitStruct.Pin = GPIO_PIN_13;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : PA1 PA4 PA5 PA6
                           PA7 PA9 PA10 PA11
                           PA12 PA15 */
  GPIO_InitStruct.Pin = GPIO_PIN_1|GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6
                          |GPIO_PIN_7|GPIO_PIN_9|GPIO_PIN_10|GPIO_PIN_11
                          |GPIO_PIN_12|GPIO_PIN_15;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PB0 PB1 PB2 PB10
                           PB11 PB12 PB13 PB14
                           PB15 PB3 PB4 PB5
                           PB6 PB7 PB8 PB9 */
  GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_10
                          |GPIO_PIN_11|GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_14
                          |GPIO_PIN_15|GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5
                          |GPIO_PIN_6|GPIO_PIN_7|GPIO_PIN_8|GPIO_PIN_9;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */
/**
 * @brief Redirect printf output calls to UART
 * @details Printf calls some functions, which finally call __io_putchar and pass formatted string
 * character by character to this function. We override this function to call HAL_UART_Transmit
 * and write character there. There's one downside - this function is blocking, that means it won't
 * return until entire character is transmitted.
 * @todo maybe override _write function too - instead getting single character we'll get pointer to
 * string and length. I believe it'll decrease HAL overhead.
 * <br> Buffering message will allow us to use UART in non-blocking mode. We do not send messages too
 * often. so casual buffer array, global flag describing if transfer is running and DMA will work
 * well. In scenario of sending messages quite often, we may think about circular buffer. It should
 * work in interrupt mode with no problems, but DMA may be tricky, especially when message continues
 * past the end of buffer array and rolls to the beginning of it.
 * @param c next single character which will be sent
 * @return Character c
 */
int __io_putchar(int c)
{
    HAL_UART_Transmit(&huart2, (uint8_t*)&c, 1, 50);
    return c;
}

/**
 * @brief set ADC measurement complete flag
 * @details This function is called when data gathered from ADC regular conversion is fully moved
 * by the DMA module to the memory.
 * @param hadc HAL ADC config struct handle
 */
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef *hadc)
{
	gAdcMeasCplt = true;
}

/**
 * @brief remaps input x from one range to another
 * @details does not constrain range. Check out Arduino "map()" reference for more details, as it
 * is the original source.
 * @param x the number to map
 * @param in_min the lower bound of the value’s current range
 * @param in_max the upper bound of the value’s current range
 * @param out_min the lower bound of the value’s target range
 * @param out_max the upper bound of the value’s target range
 * @return the mapped value
 */
const float map(const float x, const float in_min, const float in_max, const float out_min, const float out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

/**
 * @brief remaps input x from one range to another
 * @details does not constrain range.
 * Maps voltage input range to relative humidity output range
 * Check out Arduino "map()" reference for more details, as it is the original source.
 * @param x the number to map
 * @param point_lower struct with the lower bounds for current and target ranges
 * @param point_upper struct with the upper bounds for current and target ranges
 * @return the mapped value
 */
const float map(const float x, const MapPair_TypeDef point_lower, const MapPair_TypeDef point_upper)
{
	return map(x,point_lower.voltage,point_upper.voltage,point_lower.rh,point_upper.rh);
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
